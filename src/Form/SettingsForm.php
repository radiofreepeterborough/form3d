<?php

namespace Drupal\form3d\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Configure form3d settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form3d_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['form3d.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['threejs_cdn_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Threejs CDN URL'),
      '#default_value' => $this->config('form3d.settings')->get('threejs_cdn_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('form3d.settings')
      ->set('threejs_cdn_url', $form_state->getValue('threejs_cdn_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
